FROM ubuntu:16.04

MAINTAINER Nate Michael <nate@ieee.org>

RUN apt -y update
RUN apt -y --no-install-recommends install curl lib32gcc1 ca-certificates

WORKDIR /tmp

RUN curl -sSL -o steamcmd.tar.gz http://media.steampowered.com/installer/steamcmd_linux.tar.gz

RUN mkdir -p /mnt/steamcmd

RUN tar zxvf steamcmd.tar.gz -C /mnt/steamcmd

WORKDIR /mnt/steamcmd

RUN chown -R root:root /mnt

ENV HOME /mnt/steamcmd

RUN ./steamcmd.sh +login anonymous +quit

WORKDIR /
